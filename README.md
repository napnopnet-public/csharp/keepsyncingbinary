# KeepSyncingBinary

zipped binary and license
hash in powershell:  
PS C:\downloads> get-filehash KeepSyncing.exe  
  
Algorithm       Hash                                                           
---------       ----                                                            
SHA256          C2F93166CD0CC1B32DD42984A074CDB84D7B4391F053EFD6DCD0C90E0266FF33  

and:  

PS C:\downloads>  get-filehash KeepSyncing.zip  

Algorithm       Hash                                                             
---------       ----                                                            
SHA256          002D433CB4CA066EC31C2D0942E1082CF2DF24F3534BEB842DE45670F2381D67  